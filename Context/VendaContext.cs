using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class VendaContext : DbContext
    {
        public VendaContext(DbContextOptions<VendaContext> options) : base(options)
        {

        }

        public DbSet<Item> Itens { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venda>()
                .HasMany<Item>(i => i.Itens)
                .WithOne(v => v.Venda)
                .IsRequired()
                ;

            modelBuilder.Entity<Venda>()
                .HasOne(v => v.Vendedor)
                .WithOne(ve => ve.Venda)
                .HasForeignKey<Venda>(fk => fk.VendedorId)
                ;

            modelBuilder.Entity<Vendedor>()
                .HasOne(ven => ven.Venda)
                .WithOne(vend => vend.Vendedor)
                .HasForeignKey<Vendedor>(fk => fk.VendaId)
                ;

            modelBuilder.Entity<Item>()
                .HasOne(vend => vend.Venda)
                .WithMany(i => i.Itens)
                .HasForeignKey(fk => fk.VendaId)
                ;
        }
    }
}