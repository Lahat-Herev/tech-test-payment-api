using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models;

public class Item
{
    public int Id { get; set; }
    public string Nome { get; set; }
    public int VendaId { get; set; }
    public virtual Venda Venda { get; set; }
}