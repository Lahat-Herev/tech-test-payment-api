using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models;

public class Venda
{
    public int Id { get; set; }
    public DateTime Data { get; set; }
    public StatusVenda Status { get; set; }
    public int VendedorId { get; set; }
    public virtual Vendedor Vendedor { get; set; }
    public virtual List<Item> Itens { get; set; }
}