namespace tech_test_payment_api.Models;

public class ItemVenda
{
    public int ItemId { get; set; }
    public int VendaId { get; set; }
    
    public Item Item { get; set; }
    public Venda Venda { get; set; }
}