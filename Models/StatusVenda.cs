namespace tech_test_payment_api.Models
{
    public enum StatusVenda
    {
        AguardandoPagamento,
        Cancelada,
        Entregue,
        Enviada,
        PagamentoAprovado
    }
}