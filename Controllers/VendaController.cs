using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterVendaPorId(int id)
        {
            var vendaComItemEVendedor = _context.Vendas.Include(item => item.Itens).Include(vendedor => vendedor.Vendedor).Where(v => v.Id == id);
            return Ok(vendaComItemEVendedor);
        }

        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterVendaPorId), new { id = venda.Id }, venda);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendasBanco = _context.Vendas.Find(id);

            if (vendasBanco is null)
                return NotFound();

            if (vendasBanco.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da venda não pode ser vazia" });

            if (vendasBanco.Status.ToString().Equals(Enum.GetName(typeof(StatusVenda), 0))
                && venda.Status.ToString().Equals(Enum.GetName(typeof(StatusVenda), 1)) == false
                && venda.Status.ToString().Equals(Enum.GetName(typeof(StatusVenda), 4)) == false)
            {
                throw new Exception("O status de `Aguardando pagamento` só pode ser mudado para `Pagamento Aprovado` ou para `Cancelada`.");
            }

            if (vendasBanco.Status.ToString().Equals(Enum.GetName(typeof(StatusVenda), 4))
                && venda.Status.ToString().Equals(Enum.GetName(typeof(StatusVenda), 3)) == false
                && venda.Status.ToString().Equals(Enum.GetName(typeof(StatusVenda), 1)) == false)
            {
                throw new Exception("O status de `Pagamento Aprovado` só pode ser mudado para `Enviado para Transportadora` ou para `Cancelada`.");
            }

            if (vendasBanco.Status.ToString().Equals(Enum.GetName(typeof(StatusVenda), 3))
                && venda.Status.ToString().Equals(Enum.GetName(typeof(StatusVenda), 2)) == false)
            {
                throw new Exception("O status de `Enviado para Transportador` só pode ser mudado para `Entregue`.");
            }

            vendasBanco.Data = venda.Data;
            vendasBanco.Itens = venda.Itens;
            vendasBanco.Status = venda.Status;
            _context.Vendas.Update(vendasBanco);
            _context.SaveChanges();
            return Ok(vendasBanco);
        }
    }
}